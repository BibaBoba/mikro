#include <stdio.h>
#include <csignal>
#include <signal.h>
#include <unistd.h>
#include <iomanip>
#include <fcntl.h>
#include <sys/stat.h>
#include <fstream>
#include <errno.h>
#include <sys/types.h>
#include <sys/prctl.h>
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <pwd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <dirent.h>
#include <wait.h>
#include <pthread.h>
using namespace std;

enum Results {
        OK = 0
};

//SURE WORKING ZONE
string get_current_dir(){
        char* chardir = (char*)get_current_dir_name();//получаем текущий путь процесса
        string result(chardir);
        free(chardir);
        return result;
}


void do_cd(char *path){
        int result = chdir(path);//переход в директорию

}


void print_work_line(){
        uid_t effid = geteuid();
        string endsymbol= effid ==0 ? "!" : ">";//постороение строки, учитывая привелегии пользователя
        cout<< get_current_dir()<<endsymbol;
}

//SURE WORKING ZONE

//REGULAR EXPRESSIONS FILTERING
bool check(char *s,char *pattern)
{


        char *rs=0, *rp;//последняя проверенная звезда и  часть строки,котрую эта звезда поглотила
        while(1)
                if(*pattern=='*')
                        rs=s, rp=++pattern; //пройденная часть
                else if(!*s)
                        return !*pattern;
                else if(*s==*pattern || *pattern=='?')
                        ++s, ++pattern; //сдвиг в случае нахождения подслова
                else if(rs)
                        s=++rs, pattern=rp; //поглощение символа звездой
                else
                        return false;
}



vector<string> get_ALL_filenames(vector<string> regulars){

        vector<vector<string> > paths;
        for(int i=0; i<regulars.size(); i++) {//превращаем вектор строк в вектор векторов строк
                vector<string> initial;
                initial.push_back(regulars[i]);
                paths.push_back(initial);
        }
        if(paths[0][0]!="") {//дополнение начального пути текущим путем директории
                vector<string> v;
                v.push_back( get_current_dir());
                paths.insert(paths.begin(),v);

        }
        while (regulars.size()>1) {
                vector<string>prom;
                for(string i:paths[0]) {

                        DIR *dir = opendir((i+string("/")).c_str());//рассматриваем файлы в папке по пути из первого массива
                        if (dir != nullptr) {
                                for (dirent *d = readdir(dir); d != nullptr; d = readdir(dir)) {
                                     // cerr<<d->d_name;
                                        if (paths.size()>2) {// если имя папки удовлетворяет РВ, добавляем его в промежуточный массив(если рв не заканчивается, то нас интересуют тольуо папки)
                                                if(d->d_type == DT_DIR && check((char*)d->d_name,(char*)paths[1][0].c_str())) {
                                                        prom.push_back(i+string("/")+string(d->d_name));

                                                }

                                        }else{// если имя папки/файла удовлетворяет РВ, добавляем его в промежуточный массив
                                                if(check(d->d_name,(char*)paths[1][0].c_str())) {
                                                        prom.push_back(i+string("/")+ string(d->d_name));

                                                }

                                        }
                                }
                        }
                }
                paths[0] = prom;//после того как были найдены все возможные пути, заменяем первый массив на промежуточный
                paths.erase(paths.begin()+1);//удаляем только что обработанное РВ
        }
        return(paths[0]);

}




vector<string> split(string str,string symbol){
        vector<string> res;
        size_t pos =0;
        string word;
        while((pos = str.find(symbol))!= string::npos) {//пока в строке присутвует разделитель

                word = str.substr(0,pos);//выделяем слово
                res.push_back(word);//записываем в результирующий массив
                str.erase(0,pos+symbol.length());//удаляем записанное слово из строки

        }
        res.push_back(str);
        return res;
}


class Command {
public:
string comm;
vector<string> args;
string input_file="";
int ifs=-1;
string output_file="";
int ofs=-1;
Command(string command){
        vector<string> splitted = split(command," ");// разделение строки на команду и аргументы
        if(splitted[0]=="") splitted.erase(splitted.begin());
        comm = splitted[0];
        for (int i=1; i<splitted.size(); i++) {
                if(splitted[i]=="") {
                        //удаление пустых аргуметов, которые могли образоваться при разделении
                        splitted.erase(splitted.begin()+i);
                        i--;
                }else{
                        args.push_back(splitted[i]);//добавление к команде аргумента
                }
                setfiles();
        }
}
string execute(){// в зависимости от комманды выполняем ее
        string result = "ok";
        if (comm== "time") {
                result=execute_time();

        }
        else if (comm== "pwd") {
                result=execute_pwd();

        }
        else if (comm== "cd") {
                result=execute_cd();

        }else if(comm=="error") {
                result = "error";

        }else {
                result=execute_bash();

        }

        return result;
}
string redirect(){

        if(!input_file.empty()) {//если необходимо считать из файла, сменить поток ввода
                ifs = open(output_file.c_str(),O_RDONLY);
                dup2(ifs,STDIN_FILENO);
        }
        if(!output_file.empty()) {//если необходимо записать в файл, сменить поток вывода
                ofs = open(output_file.c_str(),O_WRONLY|O_TRUNC|O_CREAT,0644);
                dup2(ofs,STDOUT_FILENO);
        }
        return "OK";
}
private:
string setfiles(){
        int pos_inp=-1,pos_outp=-1;
        for (int i=0;i<args.size();i++){

            if (pos_inp = -1 && args[i] == string(">")){
                pos_inp=i;
                input_file = args[pos_inp+1];
                args.erase(args.begin()+pos_inp);
                args.erase(args.begin()+pos_inp);
                i--;
            }
            else if (pos_outp = -1 && args[i] == string("<")){
                pos_outp=i;
                output_file = args[pos_outp+1];
                args.erase(args.begin()+pos_outp);
                args.erase(args.begin()+pos_outp);
                i--;
            }
            else if(args[i]=="<"||args[i]==">"){

                comm = "error";
                break;
            }

        }
}

string execute_pwd(){
        cout<<get_current_dir()<<endl;
        return "OK";
}

string execute_cd(){
        if(args.size()>0) {
                chdir(args[0].c_str());
        }
        else{
                chdir("/home");
        }
        return "OK";
}



string execute_bash()
{
        int needchange=0;


        for (int i=0; i<args.size(); i++)
        {

                if (args[i].find("?")!=string::npos || args[i].find("*")!=string::npos ) {//проверка, содержит ли команда аргумент-РВ

                        vector <string> filenames;
                        filenames = get_ALL_filenames(split(args[i],string("/")));//получаем список всех возможных замен с учетов РВ

                        vector<char*> newargs;
                        for (int k=0; k<args.size(); k++) {
                                newargs.push_back((char*)args[k].c_str());
                        }
                        newargs.insert(newargs.begin(),(char*)comm.c_str());
                        newargs.push_back(NULL);
                        for(auto j: filenames)//выполняем команду, подставляя вместо РВ все возможные файлы
                        {

                                newargs[i+1] = (char*)j.c_str();
                                prctl(PR_SET_PDEATHSIG,SIGINT);
                                execvp(newargs[0],&newargs[0]);
                                cerr <<endl;

                        }
                        needchange =1;
                        break;

                }
        }

        if (needchange==0) {

                vector<char*> newargs;
                for (int k=0; k<args.size(); k++) {
                        newargs.push_back((char*)args[k].c_str());
                }
                newargs.insert(newargs.begin(),(char*)comm.c_str());
                newargs.push_back(NULL);
                prctl(PR_SET_PDEATHSIG,SIGINT);
                execvp(newargs[0],&newargs[0]);
        }
        return "OK";
}
string execute_time(){

        return "OK";
}

};


class Conveyor {
public:
vector<Command> conv;
Conveyor(vector<string> coms){

        for (auto i:coms) {
                Command tmp(i);
                conv.push_back(tmp);//создаем конвейер как последовательность комманд
        }
        // cout<<"Created conveyor with"<<conv.size() << "commands:"<<endl;

        for (auto i:conv) {
                // cout<<i.comm<<endl;
        }
}

string  execute()
{

        if (conv[0].comm=="time")
        {
                //time func
        }
        int standard_input = dup(STDIN_FILENO);
        int standard_output = dup(STDOUT_FILENO);
        vector<int[2]> changer(conv.size() - 1);        //массив с трубами
        int current = 0, previous = -1;
        string code;
        for (int i = 0; i < conv.size(); i++, current++, previous++)
        {

                conv[i].redirect();
                pid_t pid = fork();
                if (pid == 0)        //ребенок выполняет комманды и перенаправляет поток
                {
                        if (conv.size() == 1) {
                                code =     conv[0].execute();
                        }
                        else if (i == 0)
                        {
                                dup2(changer[0][1], STDOUT_FILENO);
                                close(changer[0][0]);
                                code=    conv[i].execute();
                        }
                        else if ((i > 0) && (i < conv.size() - 1))
                        {
                                dup2(changer[previous][0], STDIN_FILENO);
                                close(changer[previous][1]);
                                dup2(changer[current][1], STDOUT_FILENO);
                                close(changer[current][0]);
                                code=conv[i].execute();
                        }
                        else if (i == conv.size() - 1)
                        {
                                dup2(changer[previous][0], STDIN_FILENO);
                                close(changer[previous][1]);
                                code=conv[i].execute();
                        }
                        if (code!="OK") {

                                return code;
                        }
                }
                else        //родитель закрывает потоки и контролирует порядок выполнения
                {
                        if (i == conv.size() - 1)
                        {
                                for (auto &i : changer)
                                {
                                        close(i[0]);
                                        close(i[1]);
                                }
                                for (int i = 0; i < conv.size(); i++)
                                {
                                        wait(NULL);
                                }
                        }
                }
                conv[0].ifs != -1 ? close(conv[0].ifs) : 0;
                conv[0].ofs != -1 ? close(conv[0].ofs) : 0;
        }

        dup2(standard_input, STDIN_FILENO);
        dup2(standard_output, STDOUT_FILENO);
        close(standard_input);
        close(standard_output);
        return "OK";
}

};







string do_job(){
        print_work_line();//вывод текущего пути
        string input;
        getline(cin,input);
        vector<string> vec = split(input,"|");//разбиение конвейера на компоненты


        Conveyor my_conv(vec); //построение конвейера из компонент

        return my_conv.execute();//выполнение конвейера
}


int main(){
        // std::cout << "hi" << '\n';
        string r;
        //Постоянный прием и выполнение команд
        while(1) {
                r = do_job();
                if(r=="OK") {
                        // cout<<r<<endl;
                }
                else{
                    cout<<r<<endl;
                    break;}
        }
        // cout << "done" << '\n';
        return 0;
}
